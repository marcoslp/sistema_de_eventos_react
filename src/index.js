import React from "react";
import ReactDOM from "react-dom";

import "bootstrap/dist/css/bootstrap.css";
import "./global.css";

import App from "./components/App";

// const jsx = <h1>Hello, Platzi Badges!</h1>;
// const element = React.createElement(
//   "a",
//   { href: "https://platzi.com" },
//   "Ir a Platzi"
// );
// const name = "Julian";
// const element = React.createElement("h1", {}, `Hola, soy ${name}`);
// const jsx = <h1>Hola soy, {name}</h1>;
// const sum = () => 3 + 3;
// const jsx = <h1>Hola soy, {__expressiones__}</h1>;

// const jsx = (
//   <div>
//     <h1>Hola, soy Marcos</h1>
//     <p>Soy Desarrollador Front end</p>
//   </div>
// );
// const element = React.createElement(
//   "div",
//   {},
//   React.createElement("h1", {}, "Hola, soy Marcos "),
//   React.createElement("p", {}, "Soy master de la web")
// );

const container = document.getElementById("app");

// ReactDOM.render(__Que__,__donde__);
// ReactDOM.render(
//   <Badges
//     firstName="Marcos Elias"
//     lastName="Rios Nuñez"
//     jobsTitle="Desarrollador Front-End"
//     twitter="MarcosRios"
//     avatarUrl="https://i.pinimg.com/236x/8b/39/09/8b390930f9f8b93dd49d36e32876bd96.jpg"
//   />,
//   container
// );
ReactDOM.render(<App />, container);
