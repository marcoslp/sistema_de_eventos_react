import React from "react";
import { Link } from "react-router-dom";
import "./styles/NotFound.css";
import Error404 from "../images/Error404.png";
import astronautsImage from "../images/astronauts.svg";

function NotFound() {
  return (
    <React.Fragment>
      <div className="Notfound">
        <div className="container">
          <div className="row">
            <div className="NotF__col col-12 col-md-4">
              <img
                src={astronautsImage}
                alt="Astronauts"
                className="img-fluid p-4"
              />
            </div>

            <div className="NotF__col d-none d-md-block col-md-8">
              <img
                src={Error404}
                alt="Platzi Conf Logo"
                className="img-fluid mb-2"
              />
              <h1>Ups! ocurrio un error !!</h1>
              <Link className="btn btn-primary" to="/badges">
                Volver
              </Link>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
export default NotFound;
