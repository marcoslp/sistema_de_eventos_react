###Multi Page App : Por cada vez que naveguemos pedimos al servidor todos los datos, y renderizados

Ej: Pagina del clima: queremos los datos de hoy, sale consulta al servidor y vuelve con todos los datos /Hoy/Climaactual.php
si queremos pedir el clima de mañana /manaña/climaposible.php
y asi con cualquier peticion Traemos y renderizamos la pagina completa

###SPA: cuando Naveguemos por primera vez traeremos la pagina completa y renderizamos. /index.html
Pero despues queremos con consultar el clima de hoy GET/api/clima/hoy , lo hacemos a travez de una Api y solo traemos los datos del clima y no toda la pagina
si queremos los datos de mañana GET/api/clima/mañana , y va consulta y vuelve con los datos de mañana, y renderiza
si queremos los datos de pasado mañana GET/api/clima/PasadMañana y sale consulta, vuelve con los datos del clima de Pasado mañana.

###ReactRouter: Nos da las herramientas para poder hacer SPA facilmente, Usaremos 4 componentes:

    BrowserRouter: es un componente que debe estar siempre los mas arriba de la aplicacion.
    Todo lo que este adentro funcionara como una SPA
    Route: Cuando hay un match con el path "Coincidencia con la ruta", se hace render del componente.
    El componente va a recibir tres props: match, history,location.
    Switch: Dentro de Switch solamente van elementos de Route.
    Switch se asegura que solamente un route se renderize.
    Link: Toma el lugar del elemento <a>, evitando que se recargue la pagina completamente y actualiza la Url. Internamente tiene un elemento <a> pero va a interceptar el clic para navegar de manera interna sin refrescar la pagina.

Para instalar React Router lo hacemos desde la terminal con:
`npm install react-router-dom`

como es importante usar exactamente la misma version, del package.json en "dependencias" se quita lo que esta delante del 4

###Mejorando La interface de usuario
React.Fragment es la herramienta que te ayudará a renderizar varios componentes y / o elementos sin necesidad de colocar un `<div>` o cualquier otro elemento ##HTML para renderizar sus hijos. Al usar está característica de React podremos renderizar un còdigo más limpio y legible, ya que`React.Fragment` no se renderiza en el navegador.
El 404 es la ruta que se renderizará cuando ninguna otra coincida con la dirección ingresada.
Otra forma de hacer que todas tus URL's que no existen sean redirigidas a tu componente de 404 seria de la siguiente forma.

```
 Import {Redirect,Route} from "react-route-dom";
<Route path="/404" component={miComponente404}>
<redirect from="*" to="/404">
```

Como podemos observar Llamamos a nuestro componente 404 y luego utilizamos `Redirect`, el cual es un componente de ##React ##Router para hacer redirecciones. En este caso hacemos que todas las Url's que no correspondan a alguna que hayamos declarado, sean redirigidas a miComponente404

##Introducion Ciclo de Vida de un componente
Los componente cuando React los renderiza, decimos que entran en `escena`,cuando su estado cambia o recibe props diferentes se `actualizan` y cuando cambiamos de pagina y ese componente ya no esta decimos que se `Desmonto`

####Montaje:
Representa el momento donde se insera el codigo del componente en el DOM. Se llaman tres métodos:

```
Constructor: es un perfecto lugar para inicializar estados, valores.
Render: es el momento preciso donde react va a calcular lo que el elemento que representa este componente y lo va a introducir en el codigo, una vez eso ocurra React va a llamar una señal que es un metodo ComponentDidMount.
componentDidMount: cuando este es llamado nuestro componente ya es visto en pantalla, ya esta su codigo en el DOM
```

####Actualizacion:
Ocurre cuando los props o el estado del componente cambia. y vuelve a renderizarce, esto aplica a cualquier otro componente descendiente.

Se llaman dos métodos: render,componentDidUpdate.

```
ComponentDidUpdate: Recibe  2 argumentos, los props y el estado que tenia anteriormente.
Esto nos va a servir por si queremos comparar entre la version anterior del componente y la version actual.
```

####Desmontaje:
Es cuando el componente sale de escena, va a desaparecer de la pantalla.
Ej: cuando navegas de una pagina a la otra. "Ya no esta ese codigo"
Nos da la oportunidad de hacer limpieza de nuestro componente.
se llama un método: ComponentWillUnmount.

```
ComponentWillUnmount: Es el lugar perfecto para limpiar memoria, aveces estamos usando Timeout, intervals que si no cancelamos puede que perdamos memoria en componentes y eso lo vamos a querer evitar
```
